#!groovy

import groovy.json.JsonSlurper
import java.net.URL

pipeline {
  parameters {
    string(name: 'action', defaultValue: 'create', description: 'Create/update or destroy the eks cluster.')
    string(name: 'cluster', defaultValue : 'ggm-eks', description: "EKS cluster name;eg demo creates cluster named eks-demo.")
    string(name: 'region', defaultValue : 'us-east-2', description: "AWS region.")
     string(name: 'admin_users', defaultValue : 'amogh_d,govarthini_s', description: "Comma delimited list of IAM users to add to the aws-auth config map.")
}

    agent any
    options {
        timeout(time: 1, unit: 'DAYS')
        disableConcurrentBuilds()
        withAWS(credentials: 'aws_credentials', region: params.region)
        gitLabConnection('Gitlab')
    }
    stages {

    stage('Setup') {
      steps {
        script {
          currentBuild.displayName = "#" + env.BUILD_NUMBER + " " + params.action + " eks-" + params.cluster
          plan      = params.cluster + '.plan'
          prom_plan = 'prom_' + params.cluster + '.plan'
          ksm_plan  = 'ksm_' + params.cluster + '.plan' 
          ns_app_plan = 'ns_app_' + params.cluster + '.plan'
        }
      }
    }

    stage('TF Plan for EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              // Format cidrs into a list array

            sh """
              terraform workspace new ${params.cluster} || true
              terraform init
              terraform state list
              #terraform state rm module.eks_efs.null_resource.aws_auth
              terraform workspace select ${params.cluster}
              terraform plan -target module.eks_efs \
                -var cluster-name=${params.cluster} \
                -var admin_users=${params.admin_users} \
                -out ${plan}
            """
          }
        }
      }
    }

    stage('TF Apply for EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          input "Create/update Terraform stack eks-${params.cluster} in aws?" 

          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            
            sh """
              terraform apply -input=false -auto-approve ${plan}
            """
          }
        }
      }
    }

    stage('TF Plan for Prometheus in EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              // Format cidrs into a list array

            sh """
              terraform init
              terraform workspace new ${params.cluster} || true
              terraform workspace select ${params.cluster}
              terraform plan -target module.efs_promethues \
                -var cluster-name=${params.cluster} \
                -var file_system_name=${params.cluster} \
                -out ${prom_plan}
            """
          }
        }
      }
    }

    stage('TF Apply for Prometheus in EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          input "Create/update Terraform stack eks-${params.cluster} in aws?" 

          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            
            sh """
              terraform apply -input=false -auto-approve ${prom_plan}
            """
          }
        }
      }
    }

    stage('TF Plan for KubeStateMetrics in EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              // Format cidrs into a list array

            sh """
              terraform init
              terraform workspace new ${params.cluster} || true
              terraform workspace select ${params.cluster}
              terraform plan -target module.efs_kube_state_metrics \
                -out ${ksm_plan}
            """
          }
        }
      }
    }

    stage('TF Apply for KubeStateMetrics in EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          input "Create/update Terraform stack eks-${params.cluster} in aws?" 

          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            
            sh """
              terraform apply -input=false -auto-approve ${ksm_plan}
            """
          }
        }
      }
    }

    stage('TF Plan for Application in EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              // Format cidrs into a list array

            sh """
              terraform init
              terraform workspace new ${params.cluster} || true
              terraform workspace select ${params.cluster}
              terraform plan -target module.eks_namespace -target helm_release.employee \
                -var cluster-name=${params.cluster} \
                -var file_system_name=${params.cluster} \
                -out ${ns_app_plan}
            """
          }
        }
      }
    }

    stage('TF Apply for Application in EKS') {
      when {
        expression { params.action == 'create' }
      }
      steps {
        script {
          input "Create/update Terraform stack eks-${params.cluster} in aws?" 

          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
          credentialsId: 'aws_credentials', 
          accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            
            sh """
              terraform apply -input=false -auto-approve ${ns_app_plan}
            """
          }
        }
      }
    }

    stage('TF Destroy') {
      when {
        expression { params.action == 'destroy' }
      }
      steps {
        script {
          input "Destroy Terraform stack eks-${params.cluster} in aws?" 

          withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', 
            credentialsId: 'aws_credentials', 
            accessKeyVariable: 'AWS_ACCESS_KEY_ID',  
            secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {

            sh """
              terraform workspace select ${params.cluster}
              terraform init
              terraform refresh -var cluster-name=${params.cluster} -var file_system_name=${params.cluster}
              terraform destroy -auto-approve -target helm_release.employee -var cluster-name=${params.cluster} -var file_system_name=${params.cluster}
              terraform destroy -auto-approve -target module.eks_namespace -var cluster-name=${params.cluster} -var file_system_name=${params.cluster}
              terraform destroy -auto-approve -target module.efs_promethues -var cluster-name=${params.cluster} -var file_system_name=${params.cluster}
              terraform destroy -auto-approve -target module.efs_kube_state_metrics 
              terraform destroy -auto-approve -target module.eks_efs -var cluster-name=${params.cluster}
            """
          }
        }
      }
    }




    }
}
