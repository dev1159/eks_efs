module "eks_efs" {
    source = "./eks-and-efs"
    subnet_id_1 = "subnet-04e60309f42dca34e"
    subnet_id_2 = "subnet-0c69e49e37a89acb9"
    security_group_id = "sg-03c68b3e503066915"
    vpc_id = "vpc-0c9e02a15d4a3c3d2"
    route_table_id = "rtb-0099326a898bfa354"
    cluster_name = var.cluster-name
    admin_users  = var.admin_users
}

data "aws_eks_cluster_auth" "cluster" {
  //count = var.create_eks ? 1 : 0
  name  = module.eks_efs.cluster_id
}

# In case of not creating the cluster, this will be an incompletely configured, unused provider, which poses no problem.
provider "kubernetes" {
  host                   = element(concat([module.eks_efs.endpoint], list("")), 0)
  cluster_ca_certificate = base64decode(element(concat([module.eks_efs.kubeconfig-certificate-authority-data], list("")), 0))
  token                  = element(concat([data.aws_eks_cluster_auth.cluster.token], list("")), 0)
}

provider "helm" {
  kubernetes {
  host                   = element(concat([module.eks_efs.endpoint], list("")), 0)
  cluster_ca_certificate = base64decode(element(concat([module.eks_efs.kubeconfig-certificate-authority-data], list("")), 0))
  token                  = element(concat([data.aws_eks_cluster_auth.cluster.token], list("")), 0)
}
}

module "efs_promethues" {
    source = "./efs-provisioner-and-prometheus"
    efs_file_system_id = module.eks_efs.efs_file_system_id
    file_system_name = var.file_system_name
}

module "efs_kube_state_metrics" {
    source = "./kube-state-metrics-configs"
}

module "eks_namespace" {
    source = "./eks-namespace"
    namespace = "employee"
    cluster_host_url = element(concat([module.eks_efs.endpoint], list("")), 0)
    cluster_ca_cert_b64 = element(concat([module.eks_efs.kubeconfig-certificate-authority-data], list("")), 0)
    //efs_file_system_id = module.eks_efs.efs_file_system_id
    cluster_name = var.cluster-name
}

resource "helm_release" "employee" {
  name       = "employee"
  namespace  = "employee"
  chart      = "./employee"
  set {
    name  = "efs_file_system_id"
    value = module.eks_efs.efs_file_system_id
  }
  set {
    name  = "file_system_name"
    value = var.cluster-name
  }
}
