resource "null_resource" "kube_state_metrics" {
  provisioner "local-exec" {
    command     = "pwd && kubectl apply -f ./${path.module}/"
    interpreter = ["/bin/bash", "-c"]
  }
}
