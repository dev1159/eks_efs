
locals {
  kubeconfig = <<-EOF
    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority-data: ${var.cluster_ca_cert_b64}
        server: ${var.cluster_host_url}
      name: "${var.cluster_name}"
    contexts:
    - context:
        cluster: "${var.cluster_name}"
        namespace: "${var.namespace}"
        user: "${var.cluster_name}-${var.namespace}-sa"
      name: "${var.cluster_name}-${var.namespace}"
    current_context: "${var.cluster_name}-${var.namespace}"
    kind: Config
    preferences: {}
    users:
    - name: "${var.cluster_name}-${var.namespace}-sa"
      user:
        token: ${lookup(data.kubernetes_secret.this.data, "token")}
EOF

}
