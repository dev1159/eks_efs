
resource "kubernetes_role" "demo" {
  metadata {
    name      = "${var.namespace}-role"
    namespace = var.namespace
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "patch", "update", "watch"]
  }
  depends_on = [kubernetes_service_account.this]
}
