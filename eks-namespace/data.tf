
data "kubernetes_secret" "this" {
    metadata {
        name = kubernetes_service_account.this.default_secret_name
        namespace = var.namespace
    }
}
