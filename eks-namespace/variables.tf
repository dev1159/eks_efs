
variable "namespace" {
    type = string
}

variable "cluster_host_url" {
    type = string
}

variable "cluster_ca_cert_b64" {
    type = string
}

variable "cluster_name" {
    type = string
}
