resource "kubernetes_namespace" "this" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_service_account" "this" {
  metadata {
    name = var.namespace
    namespace = var.namespace
  }
}

