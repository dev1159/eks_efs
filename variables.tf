
variable "cluster-name" {
    type = string
    default = "ggm-eks"
}

variable "admin_users" {
  type = string
  default = ""
}

variable "file_system_name" {
  type = string
  default = ""
}
