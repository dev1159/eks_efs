
data "aws_vpc" "demo" {
  id = var.vpc_id
}

data "aws_subnet" "demo1" {
  id = var.subnet_id_1
}

data "aws_subnet" "demo2" {
  id = var.subnet_id_2
}

data "aws_security_group" "demo" {
  id = var.security_group_id
}

data "aws_route_table" "demo" {
   route_table_id = var.route_table_id
}

data "aws_eks_cluster_auth" "cluster" {
  //count = var.create_eks ? 1 : 0
  name  = aws_eks_cluster.demo.id
}
