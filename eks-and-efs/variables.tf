#
# Variables Configuration
#

variable "cluster_name" {
  default = "ggm-eks"
  type    = string
}

variable "subnet_id_1" {
  type = string
}

variable "subnet_id_2" {
  type = string
}

variable "security_group_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "route_table_id" {
  type = string
}

variable "admin_users" {
  type = string
}
