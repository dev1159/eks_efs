
resource "null_resource" "demo" {
  depends_on = [
    aws_eks_cluster.demo
  ]
  provisioner "local-exec" {
    command     = "aws eks update-kubeconfig --name ${var.cluster_name}"
    interpreter = ["/bin/bash", "-c"]
  }
}

resource "null_resource" "aws_auth" {
  depends_on = [
    aws_eks_cluster.demo,
    aws_eks_node_group.demo
  ]
  provisioner "local-exec" {
    command     = <<-EOT
      # Add configmap aws-auth if its not there:
      if [ ! "\$(kubectl -n kube-system get cm aws-auth 2> /dev/null)" ]
      then
          echo "Adding aws-auth configmap to ns kube-system..."
          terraform output config_map_aws_auth | awk '!/^\$/' | kubectl apply -f -
      else
          true # jenkins likes happy endings!
      fi
      if [ "${var.admin_users}" != "" ]; then
        chmod +x ./"${path.module}"/generate-aws-auth-admins.sh && ./"${path.module}"/generate-aws-auth-admins.sh "${var.admin_users}" | kubectl apply -f -
      fi
    EOT
    interpreter = ["/bin/bash", "-c"]
  }
}

resource "null_resource" "efs_driver" {
  depends_on = [
    aws_eks_cluster.demo,
    aws_iam_policy.efs_driver,
    aws_iam_role.efs_driver,
    aws_iam_role_policy_attachment.efs_driver_AmazonEKS_EFS_CSI_Driver_Policy,
    null_resource.demo
  ]
  provisioner "local-exec" {
    command     = <<EOT
      efs_deploy=`kubectl get deployments -n kube-system | grep efs-csi-controller | wc -l`
      if [ $efs_deploy -eq 0 ]; then
        kubectl apply -k 'github.com/kubernetes-sigs/aws-efs-csi-driver/deploy/kubernetes/overlays/stable/ecr/?ref=release-1.3'
      fi
    EOT
    interpreter = ["/bin/bash", "-c"]
  }
}
