#
# Elastic File System
#

resource "aws_iam_policy" "efs_driver" {
  name        = "AmazonEKS_EFS_CSI_Driver_Policy_${var.cluster_name}"
  path        = "/"
  description = "EFS CSI Driver Policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
    {
      Effect = "Allow"
      Action = [
        "elasticfilesystem:DescribeAccessPoints",
        "elasticfilesystem:DescribeFileSystems"
      ],
      Resource = "*"
    },
    {
      Effect = "Allow"
      Action = [
        "elasticfilesystem:CreateAccessPoint"
      ],
      Resource = "*",
      Condition = {
        StringLike = {
          "aws:RequestTag/efs.csi.aws.com/cluster" = "true"
        }
      }
    },
    {
      Effect = "Allow",
      Action = "elasticfilesystem:DeleteAccessPoint",
      Resource = "*",
      Condition = {
        StringEquals = {
          "aws:ResourceTag/efs.csi.aws.com/cluster" = "true"
        }
      }
    }
  ]
  })
}

resource "aws_iam_role" "efs_driver" {
  name = "AmazonEKS_EFS_CSI_DriverRole_${var.cluster_name}"

  assume_role_policy = jsonencode({
  Version = "2012-10-17"
  Statement = [
    {
      Effect = "Allow"
      Principal = {
        Federated = "arn:aws:iam::${local.aws_account_id}:oidc-provider/${local.eks_cluster_issuer}"
      },
      Action = "sts:AssumeRoleWithWebIdentity"
      Condition = {
        StringEquals = {
          "${local.eks_cluster_issuer}:sub": "system:serviceaccount:kube-system:efs-csi-controller-sa"
        }
      }
    }]
  })
}

resource "aws_iam_role_policy_attachment" "efs_driver_AmazonEKS_EFS_CSI_Driver_Policy" {
  policy_arn = "arn:aws:iam::${local.aws_account_id}:policy/${aws_iam_policy.efs_driver.name}"
  role       = aws_iam_role.efs_driver.name
}

resource "kubernetes_service_account" "efs_driver" {
  metadata {
    name = "efs-csi-controller-sa"
    namespace = "kube-system"
    labels = {
      "app.kubernetes.io/name" = "aws-efs-csi-driver"
    }
    annotations = {
      "eks.amazonaws.com/role-arn" = "arn:aws:iam::${local.aws_account_id}:role/${aws_iam_role.efs_driver.name}"
    }
  }
  timeouts {
   create = "50s"
 }
}

resource "aws_efs_file_system" "demo" {
  creation_token = var.cluster_name
  tags = {
    Name = "EKS"
  }
  depends_on = [
    null_resource.efs_driver
  ]
}

resource "aws_efs_mount_target" "demo" {
  file_system_id = aws_efs_file_system.demo.id
  subnet_id      = data.aws_subnet.demo1.id
  security_groups = ["${data.aws_security_group.demo.id}"]
}

resource "aws_efs_mount_target" "demo1" {
  file_system_id = aws_efs_file_system.demo.id
  subnet_id      = data.aws_subnet.demo2.id
  security_groups = ["${data.aws_security_group.demo.id}"]
}

resource "aws_efs_access_point" "demo" {
  file_system_id = aws_efs_file_system.demo.id
}
