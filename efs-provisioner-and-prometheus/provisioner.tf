resource "null_resource" "prometheus" {
  provisioner "local-exec" {
    command     = <<-EOT
        kubectl create namespace prometheus
        helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
        helm repo update
        helm upgrade -i prometheus prometheus-community/prometheus \
            --namespace prometheus \
            --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2",server.service.type="LoadBalancer",server.service.servicePort="9090"
    EOT
    interpreter = ["/bin/bash", "-c"]
  }

  provisioner "local-exec" {
    when        = destroy
    command     = <<-EOT
        helm uninstall prometheus --namespace prometheus
        kubectl delete namespace prometheus
    EOT
    interpreter = ["/bin/bash", "-c"]
  }
}

