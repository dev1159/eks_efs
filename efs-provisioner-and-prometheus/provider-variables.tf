
data "aws_efs_file_system" "demo" {
    creation_token = var.file_system_name
}

output "demo" {
    value = data.aws_efs_file_system.demo
}

resource "kubernetes_namespace" "demo" {
  metadata {
    name = var.namespace
  }
}
