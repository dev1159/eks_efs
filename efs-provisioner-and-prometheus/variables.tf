variable "efs_file_system_id" {
    type = string
}

variable "file_system_name" {
    type = string
}

variable "namespace" {
    default = "prom-graf"
}
